from django.db import models
from trails.models import Trail
from django.contrib.auth import get_user_model

User = get_user_model()

# Create your models here.


class Restaurant(models.Model):
    restaurant_name = models.CharField(max_length=200)
    visited = models.BooleanField(default=False)
    trail = models.ForeignKey(
        Trail,
        related_name="tasks",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.restaurant_name

    user = models.ForeignKey(
        User,
        related_name="tasks",
        on_delete=models.CASCADE,
    )
