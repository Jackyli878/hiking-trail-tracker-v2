# Generated by Django 4.2 on 2023-05-04 18:47

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("restaurant", "0001_initial"),
    ]

    operations = [
        migrations.RenameField(
            model_name="restaurant",
            old_name="assignee",
            new_name="user",
        ),
    ]
