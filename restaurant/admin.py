from django.contrib import admin
from restaurant.models import Restaurant

# Register your models here.


@admin.register(Restaurant)
class RestaurantAdmin(admin.ModelAdmin):
    list_display = (
        "restaurant_name",
        "visited",
        "trail",
        "user",
    )
