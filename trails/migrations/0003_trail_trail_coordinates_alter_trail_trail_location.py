# Generated by Django 4.2 on 2023-05-05 05:05

from django.db import migrations
import django_google_maps.fields


class Migration(migrations.Migration):
    dependencies = [
        ("trails", "0002_trail_trail_location"),
    ]

    operations = [
        migrations.AddField(
            model_name="trail",
            name="trail_coordinates",
            field=django_google_maps.fields.GeoLocationField(
                max_length=100, null=True
            ),
        ),
        migrations.AlterField(
            model_name="trail",
            name="trail_location",
            field=django_google_maps.fields.AddressField(
                max_length=200, null=True
            ),
        ),
    ]
