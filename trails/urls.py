from django.urls import path
from trails.views import trail_list

urlpatterns = [
    path("", trail_list, name="trail_list"),
]
