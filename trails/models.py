from django.db import models
from django.contrib.auth import get_user_model


User = get_user_model()

# Create your models here.


class Trail(models.Model):
    trail_name = models.CharField(max_length=200)
    trail_county = models.CharField(max_length=200)
    trail_distance = models.PositiveSmallIntegerField()
    user = models.ForeignKey(
        User,
        related_name="trail",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return self.trail_name
