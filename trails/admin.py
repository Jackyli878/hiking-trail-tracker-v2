from django.contrib import admin
from trails.models import Trail


# Register your models here.
@admin.register(Trail)
class TrailAdmin(admin.ModelAdmin):
    list_display = (
        "trail_name",
        "trail_county",
        "trail_distance",
    )
