from django.shortcuts import render, get_object_or_404, redirect
from restaurant.models import Trail
from django.contrib.auth.decorators import login_required

# Create your views here.


def trail_list(request):
    trail_instance = Trail.objects.filter(user=request.user.id)
    context = {
        "trail_instance": trail_instance,
    }
    return render(request, "trails/list.html", context)
